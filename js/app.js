function pushArr(arr, parent) {
    if (!parent) {
        for (let i = 0; i < arr.length; i++) {
            let li = document.createElement('li');
            li.textContent = arr[i]; 
            document.body.append(li);
        }
    } else {
        const group = document.querySelector(`.${parent}`);
        for (let i = 0; i < arr.length; i++) {
          let li = document.createElement('li');
          li.textContent = arr[i]; 
          group.append(li);
        }
    }

}

newArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", 5];
pushArr(newArr, 'header')

